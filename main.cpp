
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>


typedef std::vector<bool> bitset;
bitset num_to_bitset (int num) {
    bitset vec;
    while(num) {
        if (num&1)
            vec.push_back(1);
        else
            vec.push_back(0);
        num>>=1;
    }
    std::reverse(vec.begin(),vec.end());
    
    return vec; 
}

bitset merge(const bitset& a, const bitset& b) {
    if (b.size() == 0) {
        return a;
    }

    bitset res = a;

    res.reserve(res.size() + b.size());
    res.insert(res.end(), b.begin(), b.end());
    return res;
}

// Trimming zeros from beginning of vector
bitset trim_zero(bitset a) {
    int i = 0;
    for (const auto& e : a) {
        if (e == 0) {
            i++;
        } else {
            break;
        }
    }
    return bitset(a.begin() + i, a.end());
}

// Adding zeros at the end of the vector until vector's size == size
bitset add_zeros(const bitset& a, int size) {
    bitset res;
    for (size_t i = 0; i < size - a.size(); i++)
    {
        res.emplace_back(0);
    }
    return merge(res, a);
}

// Calculate XOR operation for 2 polynoms (used as a step of polynoms division calculation)
bitset perform_division(bitset a, bitset b) {
    if (a.size() != b.size()) {
        throw "Error 1";
    }
    bitset res;
    for (size_t i = 0; i < a.size(); i++)
    {
        auto op = !a[i] != !b[i];
        res.emplace_back(op);
    }
    res = trim_zero(res);
    return res;
}

// Divides one polynom by another. Returns remainder
bitset divide(bitset a, bitset b) {
    a = trim_zero(a);
    bitset current(a.begin(), a.begin() + b.size());
    bitset rest(a.begin() + b.size(), a.end());
    auto itr = a.begin() + b.size();
    do {
        bitset remain(itr, a.end());
        current = perform_division(current, b);
        auto toAdd = b.size() - current.size();
        if (remain.size()  < toAdd) {
            current = merge(current, remain);
            return current;
        }
        auto itrNew = itr + toAdd;
        bitset addition(itr, itrNew);
        current = merge(current, addition);
        itr = itrNew;
    } while(true);
}

void print_bitset(bitset vec, std::string prefix = "", std::string delimeter = "") {
    std::cout << prefix;

    for (const auto& e: vec) {
        std::cout << e << delimeter;
    }

    std::cout << std::endl;
}

int main() {
    int gxNum = 0b111010001; // BCH (15,7)
    std::vector<bool> gx = num_to_bitset(gxNum);
    while (true) {
        int symbol = 0;
        std::cout << "Enter symbol to code (number representation): ";
        std::string symbolString;
        std::cin >> symbolString;
        symbol = std::stoi(symbolString);
        // symbol = 8;

        std::vector<bool> A1 = num_to_bitset(symbol);
        if (A1.empty()) {
            std::cout << "0 is not allowed" << std::endl;
            continue;
        }
        if (A1.size() > 7) {
            std::cout << "Error, max info bits: 7, actual: " << A1.size() << std::endl;
            continue;
        }
        print_bitset(add_zeros(A1, 7), "Value as bits: ");

        // Adding empty bits at the end. These bits are control and will be filled later
        bitset multiplier = {0,0,0,0,0,0,0,0};
        auto multiplied = merge(A1, multiplier);
       
        // Dividing target polynom by gx to get a ramainder that will represent control bits
        auto R1 = divide(multiplied, gx);
        // Adding additional required zeros at the begginning
        R1 = add_zeros(R1, multiplier.size());
        print_bitset(R1, "R1(x): ");

        // Merging info bits with control bits
        auto res = merge(A1, R1);
        auto bch_combination_size = 15;
        // Adding aditional prefix zeros because we are using bch 15
        res = add_zeros(res, bch_combination_size);
        print_bitset(res, "Calculated combination: ");
    }


    return 0;
}
